## AWS-CLOUD-INFA

### 1. Network and Backend
![image](images/SIVA-BACKENDFLOW.png)

### 2. CICD Flow
![image](images/SIVA-CICD.png)
#### 2.1 Get key for mirroring Gitlab to AWS
1. Go to [Iam](https://us-east-1.console.aws.amazon.com/iam/home?region=us-east-2#/users/details/admin?section=security_credentials) -> User -> Security credentials
2. Scroll to HTTPS Git credentials for AWS CodeCommit
3. Generate credentials and save them

#### 2.2 How to apply new project
1. Create new folder in [projects](modules/projects) then create codepipelines hcl file
2. In modules/projects create new project folder and teraform in there
3. cd to folder which includes hcl file
4. Run this command: terragunt plan -> terragrunt apply -> check new resources -> yes/no
5. Get Repository [URL](https://us-east-2.console.aws.amazon.com/codesuite/codecommit/repositories?region=us-east-2) from CodeCommit: CodeCommit -> Your Project -> Clone URL -> Clone HTTPS
6. Go to your repository in Gitlab -> setting -> repository -> mirroring repositories -> expand -> add new
    * In Git repository URL: Paste URL step 5
    * Authen method: Username and password
    * Username and password get from 2.1
7. Go to CodeCommit and check whatever code is there

If you just migrate your repository to Codecommit, you can stop at step 7. And if you want build ECR follow next step
8. Go to [terraform-aws-ecr](modules/system/terraform-aws-ecr) add your project name to [ecr_repositories.json](modules/system/terraform-aws-ecr/ecr_repositories.json)
9. cd to [ecr](enviroments/develop/system/ecr) which includes terragrunt file
10. Run cmd: terragrunt apply -> yes
### 3. Development
#### 3.1 Get key for developing
1. Go to [Iam](https://us-east-1.console.aws.amazon.com/iam/home?region=us-east-2#/users/details/admin?section=security_credentials) -> User -> Security credentials
2. Scroll to Access keys
3. Generate credentials and save them

#### 3.2 How to apply new project with ECS cluster
**Follow step in 4.1 from step 4 to step 11**

### 4 Apply for new AWS account or enviroment
#### 4.1 For the [system](enviroments/develop/system)
1. Create project and structure folder like this
2. Apply this [terragrunt](terragrunt.hcl) file, it will generate 1 S3 bucket, 1 DynamoDB table. The bucket and Dynamo table will control version and all resources can inherit.
3. Apply [S3](enviroments/develop/system/s3) Raw bucket for storage log pipline, log build, cloudwatch.
4. Apply [Iam](enviroments/develop/system/iam) to create role for CodeBuild, Task for ECS,...
5. Apply [sg](enviroments/develop/system/sg) create security group for traffic in your default VPC.
6. Apply [template](enviroments/develop/system/template) this template for auto-scaling work on and task in ecs will run docker images in this template.
7. Apply [auto-scaling](enviroments/develop/system/auto-scaling) this config number of cluster, how ecs-cluster expand.
8. Apply [ecr](enviroments/develop/system/ecr) for storage images.
9. Apply [lb](enviroments/develop/system/lb) for creating entry point, route traffic from image to internet.
10. Apply [endpoint](enviroments/develop/system/endpoint) create endpoint s3 and dynamodb, this will avoid traffic go out your VPC.
11. Apply [ecs](enviroments/develop/system/ecs) create cluster, define service, define task for your services.

#### 4.2 For [project](enviroments/develop/projects)
1. If you want mirror your repository from local to AWS, you will go **2.2 How to apply new project**

#### 4.3 CMD in terragrunt
1. terragrunt init -> init cache, lock file, create statement.
2. terragrunt plan -> show resources that you intend to apply.
3. terragrunt apply -> apply resources.
4. terragrunt force-unlock **status-id** -> terragurnt force only 1 person can modify 1 resource, others people will be lock until resource be modified done.
5. terragrunt apply -target aws_iam_role.codepipeline_role -target ... -> apply a resource or multiple resource selected.