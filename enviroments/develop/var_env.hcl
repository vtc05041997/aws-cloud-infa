# Set account-wide variables. These are automatically pulled in to configure the remote state bucket in the root
# terragrunt.hcl configuration.

locals {
  account_name          = "aws-siva"
  aws_account_id        = "711492413661"
  aws_profile           = "terraform-siva"
  aws_iam_sso_role_name = "AWSReservedSSO_AdministratorAccess_5f626a63babcdc1f"
  environment           = "develop"
  environment_code      = "develop"

  aws_region      = "us-east-2"
  aws_region_code = "uset2"

  master_prefix   = "system"
  database_prefix = "system"
}