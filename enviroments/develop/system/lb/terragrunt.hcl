locals {
  var_env = read_terragrunt_config(find_in_parent_folders("var_env.hcl"))

  # Extract out common variables for reuse
  account_name          = local.var_env.locals.account_name
  aws_account_id        = local.var_env.locals.aws_account_id
  aws_profile           = local.var_env.locals.aws_profile
  aws_iam_sso_role_name = local.var_env.locals.aws_iam_sso_role_name
  environment           = local.var_env.locals.environment
  environment_code      = local.var_env.locals.environment_code
  aws_region            = local.var_env.locals.aws_region
  aws_region_code       = local.var_env.locals.aws_region_code
  database_prefix       = local.var_env.locals.database_prefix
  master_prefix         = local.var_env.locals.master_prefix
  infa                  = "lb"

}

#To inherit this configuration, in each of the child terragrunt.hcl files, 
#You can tell Terragrunt to automatically include all the settings from the root terragrunt.hcl file as follows:
#Flow the doccument: https://terragrunt.gruntwork.io/docs/features/keep-your-remote-state-configuration-dry/
include "root" {
  path = find_in_parent_folders()
}

terraform {
  source = "../../../../modules/system//terraform-aws-lb"
}

dependency sg {
  config_path = "../sg"
}


# These are the variables we have to pass in to use the module specified in the terragrunt configuration above
inputs = {

  security_groups = [dependency.sg.outputs.security_group_api_id]
  #Siva public subnet and vpc
  vpc_id          = "vpc-0ee7902ac393eb806"
  public_subnets  = ["subnet-00cfa1183be73110f", "subnet-0c3a12c253dcc1b6c", "subnet-01da92023badcbe06"]
  env             = local.environment

  tags = {
    "Terraform" : true
    "siva:environment-name" : local.environment
    "siva:aws-region" : local.aws_region
    "siva:infa" : local.infa
  }
}
