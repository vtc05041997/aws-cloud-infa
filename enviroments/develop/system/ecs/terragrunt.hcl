locals {
  var_env = read_terragrunt_config(find_in_parent_folders("var_env.hcl"))

  # Extract out common variables for reuse
  account_name          = local.var_env.locals.account_name
  aws_account_id        = local.var_env.locals.aws_account_id
  aws_profile           = local.var_env.locals.aws_profile
  aws_iam_sso_role_name = local.var_env.locals.aws_iam_sso_role_name
  environment           = local.var_env.locals.environment
  environment_code      = local.var_env.locals.environment_code
  aws_region            = local.var_env.locals.aws_region
  aws_region_code       = local.var_env.locals.aws_region_code
  database_prefix       = local.var_env.locals.database_prefix
  master_prefix         = local.var_env.locals.master_prefix
  infa                  = "ecs"

}

#To inherit this configuration, in each of the child terragrunt.hcl files, 
#You can tell Terragrunt to automatically include all the settings from the root terragrunt.hcl file as follows:
#Flow the doccument: https://terragrunt.gruntwork.io/docs/features/keep-your-remote-state-configuration-dry/
include "root" {
  path = find_in_parent_folders()
}

terraform {
  source = "../../../../modules/system//terraform-aws-ecs"
}

dependency autoscaling {
  config_path = "../auto-scaling"
}

dependency iam {
  config_path = "../iam"
}

dependency ecr {
  config_path = "../ecr"
}

dependency sg {
  config_path = "../sg"
}

dependency lb {
  config_path = "../lb"
}

# These are the variables we have to pass in to use the module specified in the terragrunt configuration above
inputs = {

  security_groups = [dependency.sg.outputs.security_group_api_id]
  env             = local.environment


  user_management_api_asg_arn      = dependency.autoscaling.outputs.user_management_api_asg_arn
  iam_role_ecs_task_execution_arn  = dependency.iam.outputs.ecs_task_execution_role_arn
  public_subnets                   = ["subnet-00cfa1183be73110f", "subnet-0c3a12c253dcc1b6c", "subnet-01da92023badcbe06"]
  map_name_url_ecr                 = dependency.ecr.outputs.map_name_url_ecr

  # For API Gateway Service
  um_api_gateway_service_lb_tg     = dependency.lb.outputs.um_api_gateway_service_tg
  um_service_discovery_lb_tg       = dependency.lb.outputs.um_service_discovery_tg
  um_ui_lb_tg                      = dependency.lb.outputs.um_ui_tg
  um_user_registration_lb_tg       = dependency.lb.outputs.um_user_registration_tg

  tags = {
    "Terraform" : true
    "savi:environment-name" : local.environment
    "savi:aws-region" : local.aws_region
    "savi:infa" : local.infa
  }
}
