output "security_group_api_id" {
  value = aws_security_group.security_group_api.id
}

output "security_group_api_arn" {
  value = aws_security_group.security_group_api.arn
}