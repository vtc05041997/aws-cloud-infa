output "user_management_api_template_name" {
  value = aws_launch_template.user_management_api_template.name
}

output "user_management_api_template_id" {
  value = aws_launch_template.user_management_api_template.id
}