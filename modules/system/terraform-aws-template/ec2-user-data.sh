#!/bin/bash
# Use this for your user data (script from top to bottom)
# install httpd (Linux 2 version)
sudo yum update -y
sudo amazon-linux-extras enable corretto8
sudo yum install java-1.8.0-amazon-corretto -y
sudo yum install -y httpd
sudo yum install awscli -y
systemctl start httpd
systemctl enable httpd
echo "<h1>Hello World from $(hostname -f)</h1>" > /var/www/html/index.html
echo ECS_CLUSTER=system-develop-ecs-cluster >> /etc/ecs/ecs.config
# echo "install codedeploy-agent"
# sudo yum install ruby -y
# sudo yum install wget -y
# cd /home/ec2-user
# wget https://aws-codedeploy-ap-southeast-1.s3.ap-southeast-1.amazonaws.com/latest/install
# chmod +x ./install
# ./install auto
# service codedeploy-agent status
# service codedeploy-agent start
# service codedeploy-agent status
# sudo yum install amazon-cloudwatch-agent -y
# service awslogs status
# service awslogs start
# service awslogs status