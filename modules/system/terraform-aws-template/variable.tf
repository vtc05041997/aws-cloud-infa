variable "image_id" {
  type = string
}

variable "instance_type" {
  type = string
}

variable "security_groups" {
  type = set(string)
}

variable "instance_keypair" {
  type = string
}

variable "aws_iam_instance_profile_arn" {
  type = string
}

variable "env" {
  type = string
}

variable "tags" {
  type = map(string)
}