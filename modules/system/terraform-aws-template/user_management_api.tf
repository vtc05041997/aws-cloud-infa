resource "aws_launch_template" "user_management_api_template" {
  name          = format("user-management-api-%s-template", var.env)
  image_id      = var.image_id
  instance_type = var.instance_type

  vpc_security_group_ids = var.security_groups
  key_name               = var.instance_keypair
  iam_instance_profile {
    arn = var.aws_iam_instance_profile_arn
  }

  user_data = filebase64("ec2-user-data.sh")

  # ebs_optimized = true
  # default_version = 1
  # update_default_version = true
  # block_device_mappings {
  #   device_name = "/dev/sda1"
  #   ebs {
  #     volume_size = 10
  #     #volume_size = 20 # LT Update Testing - Version 2 of LT      
  #     delete_on_termination = true
  #     volume_type           = "gp2" # default is gp2
  #   }
  # }

  monitoring {
    enabled = true
  }

  tag_specifications {
    resource_type = "instance"
    tags = {
      Name = format("user-management-api-%s-template", var.env)
    }
  }
}