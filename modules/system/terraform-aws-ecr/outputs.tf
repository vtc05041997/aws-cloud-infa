output "all_instances" {
  value       = aws_ecr_repository.user_management_api_ecr
  description = "ECR details"
}

output "map_name_url_ecr" {
  value = { for s in aws_ecr_repository.user_management_api_ecr : s.id => s.repository_url}
}