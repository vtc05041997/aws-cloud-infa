locals {
  ecr_list = jsondecode(file("ecr_repositories.json"))
  #   ec2_list = jsondecode(file("${path.root}/${var.ec2_file_name}"))
}

resource "aws_ecr_repository" "user_management_api_ecr" {
  for_each = {for repository in local.ecr_list : repository.name => repository}
  name                 = format("%s-%s-ecr", each.value.name, var.env)
  image_tag_mutability = "MUTABLE"

  image_scanning_configuration {
    scan_on_push = true
  }
  
  tags = merge(var.tags)
}
