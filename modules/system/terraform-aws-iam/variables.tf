variable "name" {
  description = "Name for Iam User"
  default     = "jarvis-{environment}"
  type        = string
}

variable "tags" {
  description = "A map of tags to add to all resources"
  type        = map(string)
}

variable "raw_bucket_name" {
  type        = string
  description = "raw_bucket_name"
}