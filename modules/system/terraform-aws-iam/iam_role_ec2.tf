data "aws_iam_policy" "full_s3_access" {
  name = "AmazonS3FullAccess"
}

data "aws_iam_policy" "ecs_container" {
  name = "AmazonEC2ContainerServiceforEC2Role"
}

resource "aws_iam_instance_profile" "ec2_instance_profile" {
  name = format("%s-instance-profile-role", var.name)
  role = aws_iam_role.ec2_role.name
}

resource "aws_iam_role" "ec2_role" {
  name = format("%s-ec2-role", var.name)
  assume_role_policy = jsonencode({
    "Version" : "2012-10-17",
    "Statement" : [
      {
        "Effect" : "Allow",
        "Principal" : {
          "Service" : "ec2.amazonaws.com"
        },
        "Action" : "sts:AssumeRole"
      }
    ]
  })
  tags = merge(
    {
      "Name" = format("%s-ec2-role", var.name)
    },
    var.tags
  )
}

resource "aws_iam_policy" "ec2_additional_policy_for_ecs" {
  name = format("%s-ecs-policy", var.name)

  policy = jsonencode({
    "Version" : "2012-10-17",
    "Statement" : [
      {
        "Effect" : "Allow",
        "Action" : [
          "logs:CreateLogGroup",
          "logs:CreateLogStream",
          "logs:PutLogEvents",
          "logs:DescribeLogStreams"
        ],
        "Resource" : [
          "arn:aws:logs:*:*:*",
        ]
      }
    ]
    }
  )
  tags = merge(
    var.tags
  )
}

resource "aws_iam_role_policy_attachment" "ec2_additional_policy_for_ecs" {
  role       = aws_iam_role.ec2_role.name
  policy_arn = aws_iam_policy.ec2_additional_policy_for_ecs.arn
}

resource "aws_iam_role_policy_attachment" "full_s3_access" {
  role       = aws_iam_role.ec2_role.name
  policy_arn = data.aws_iam_policy.full_s3_access.arn
}

resource "aws_iam_role_policy_attachment" "ecs_container" {
  role       = aws_iam_role.ec2_role.name
  policy_arn = data.aws_iam_policy.ecs_container.arn
}