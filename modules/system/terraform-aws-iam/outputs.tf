
output "codebuild_role_arn" {
  value = aws_iam_role.codebuild_role.arn
}

output "codepipeline_role_arn" {
  value = aws_iam_role.codepipeline_role.arn
}

output "codedeploy_role_arn" {
  value = aws_iam_role.codedeploy_role.arn
}

output "ec2_role_arn" {
  value = aws_iam_role.ec2_role.arn
}

output "ec2_role_name" {
  value = aws_iam_role.ec2_role.name
}

output "aws_iam_instance_profile_arn" {
  value = aws_iam_instance_profile.ec2_instance_profile.arn
}

output "aws_iam_instance_profile_name" {
  value = aws_iam_instance_profile.ec2_instance_profile.name
}

output "ecs_task_execution_role_arn" {
  value = aws_iam_role.ecs_task_execution_role.arn
}

output "ecs_task_execution_role_name" {
  value = aws_iam_role.ecs_task_execution_role.name
}