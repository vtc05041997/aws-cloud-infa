# ################################################################################
# # Gateway Endpoint
# ################################################################################
locals {
  endpoints_list = jsondecode(file("gateway_endpoint.json"))
  #   ec2_list = jsondecode(file("${path.root}/${var.ec2_file_name}"))
}

resource "aws_vpc_endpoint" "this" {
  for_each          = { for endpoint in local.endpoints_list : endpoint.service_name => endpoint }
  vpc_id            = var.vpc_id
  service_name      = "com.amazonaws.${data.aws_region.current.name}.${each.value.service_name}"
  vpc_endpoint_type = "Gateway"
  route_table_ids   = var.public_route_table_id

  # subnet_ids        = var.public_subnet_id # Only for Interface AND gatwayloadbalance 
  # security_group_ids = var.security_group_ids get default
  # private_dns_enabled = true # Only for Interface

  tags = merge(
    var.tags,
    {
      "Name" = "${var.name}-vce-${each.value.service_name}"
    },
  )
}