
variable "user_management_api_asg_arn" {
  type = string
}

variable "iam_role_ecs_task_execution_arn" {
  type = string
}

variable "um_api_gateway_service_lb_tg" {
  type = string
}

variable "um_service_discovery_lb_tg" {
  type = string
}

variable "um_ui_lb_tg" {
  type = string
}

variable "um_user_registration_lb_tg" {
  type = string
}

variable "map_name_url_ecr" {
  type = map(string)
}

variable "public_subnets" {
  type = set(string)
}

variable "security_groups" {
  type = set(string)
}

variable "env" {
  type = string
}

variable "tags" {
  type = map(string)
}