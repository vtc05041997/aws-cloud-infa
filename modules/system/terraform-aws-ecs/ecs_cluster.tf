resource "aws_ecs_cluster" "origin_cluster" {
  name = format("system-%s-ecs-cluster", var.env)
}

resource "aws_ecs_capacity_provider" "user_management_api_capacity" {
  name = format("user-management-api-%s-ecs-capacity", var.env)

  auto_scaling_group_provider {
    auto_scaling_group_arn = var.user_management_api_asg_arn

    # If using auto scaling group then should'nt use manage scaling
    # managed_scaling {
    #   maximum_scaling_step_size = 100
    #   minimum_scaling_step_size = 1
    #   status                    = "ENABLED"
    #   target_capacity           = 3
    # }
  }
}

resource "aws_ecs_cluster_capacity_providers" "user_management_api_providers" {
  cluster_name = aws_ecs_cluster.origin_cluster.name

  capacity_providers = [aws_ecs_capacity_provider.user_management_api_capacity.name]

  default_capacity_provider_strategy {
    base              = 1
    weight            = 100
    capacity_provider = aws_ecs_capacity_provider.user_management_api_capacity.name
  }
}