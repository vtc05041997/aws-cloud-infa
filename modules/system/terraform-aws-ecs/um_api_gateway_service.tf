data "aws_ecr_image" "api_gateway_service" {
  repository_name = format("um-api-gateway-service-%s-ecr", var.env)
  image_tag       = "latest"
}
resource "aws_ecs_task_definition" "um_api_gateway_service_ecs_task" {
  family             = format("um-api-gateway-service-%s-ecs-task", var.env)
  network_mode       = "awsvpc"
  execution_role_arn = var.iam_role_ecs_task_execution_arn
  task_role_arn      = var.iam_role_ecs_task_execution_arn
  memory             = 512
  runtime_platform {
    operating_system_family = "LINUX"
    cpu_architecture        = "X86_64"
  }

  container_definitions = jsonencode([
    {
      name      = format("um-api-gateway-service-%s-docker-container", var.env)
      image     = format("%s:latest@%s", var.map_name_url_ecr[format("um-api-gateway-service-%s-ecr", var.env)], data.aws_ecr_image.api_gateway_service.image_digest)
      cpu       = 256
      memory    = 512
      essential = true

      portMappings = [
        {
          containerPort = 8080
          hostPort      = 8080
          protocol      = "tcp"
        }
      ],

      logConfiguration = {
        logDriver = "awslogs"
        options = {
          awslogs-region        = data.aws_region.current.name
          awslogs-group         = aws_cloudwatch_log_group.um_api_gateway_service_cwl_gr.name
          awslogs-stream-prefix = "ecs"
        }
      }

    }
  ])
}

resource "aws_ecs_service" "um_api_gateway_service_ecs_service" {
  name            = format("um-api-gateway-service-%s-ecs-service", var.env)
  cluster         = aws_ecs_cluster.origin_cluster.id
  task_definition = aws_ecs_task_definition.um_api_gateway_service_ecs_task.arn
  desired_count   = 1

  network_configuration {
    subnets          = var.public_subnets
    security_groups  = var.security_groups
    # assign_public_ip = true
  }

  force_new_deployment = true
  placement_constraints {
    type = "distinctInstance"
  }

  triggers = {
    redeployment = timestamp()
  }

  capacity_provider_strategy {
    capacity_provider = aws_ecs_capacity_provider.user_management_api_capacity.name
    weight            = 100
  }

  load_balancer {
    target_group_arn = var.um_api_gateway_service_lb_tg
    container_name   = format("um-api-gateway-service-%s-docker-container", var.env)
    container_port   = 8080
  }
}

resource "aws_cloudwatch_log_group" "um_api_gateway_service_cwl_gr" {
  name = format("um-api-gateway-service-%s-cwl-gr", var.env)

  tags = merge(
    {
      "Name" = format("um-api-gateway-service-%s-cwl-gr", var.env)
    },
    var.tags
  )
}