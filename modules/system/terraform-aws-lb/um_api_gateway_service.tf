resource "aws_lb" "um_api_gateway_service_lb" {
  name               = format("user-api-management-%s-lb", var.env)
  internal           = false
  load_balancer_type = "application"
  security_groups    = var.security_groups
  idle_timeout       = 60
  subnets            = var.public_subnets

  #enable_cross_zone_load_balancing = true alway ON alb no charge, off nlb and charge if on

  tags = merge(
    {
      "Name" = format("user-api-management-%s-lb", var.env)
    },
    var.tags
  )
}

resource "aws_lb_listener" "http" {
  load_balancer_arn = aws_lb.um_api_gateway_service_lb.arn
  port              = "8080"
  protocol          = "HTTP"

  # ssl_policy        = "ELBSecurityPolicy-2016-08"
  # certificate_arn   = "arn:aws:iam::187416307283:server-certificate/test_cert_rab3wuqwgja25ct3n4jdj2tzu4"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.um_api_gateway_service_tg.arn
  }
}

resource "aws_lb_target_group" "um_api_gateway_service_tg" {
  name     = format("um-api-gtw-srv-%s-lb", var.env)
  port     = 8080
  protocol = "HTTP"
  vpc_id   = var.vpc_id

  target_type = "ip" # For ECS tasks you need to use IP to suiable mode "awsvpc" scale out
  
  health_check {
    # enabled             = true
    path                = "/healcheck"
    port                = "8080"
    # protocol            = "HTTP"
    # healthy_threshold   = 3
    # unhealthy_threshold = 2
    # interval            = 30
    # timeout             = 10
    matcher             = "200"
  }

  # depends_on = [aws_lb.alb]

  tags = merge(
    {
      "Name" = format("um-api-gateway-service-%s-tg", var.env)
    },
    var.tags
  )
}

resource "aws_lb_listener" "http_8081" {
  load_balancer_arn = aws_lb.um_api_gateway_service_lb.arn
  port              = "8081"
  protocol          = "HTTP"

  # ssl_policy        = "ELBSecurityPolicy-2016-08"
  # certificate_arn   = "arn:aws:iam::187416307283:server-certificate/test_cert_rab3wuqwgja25ct3n4jdj2tzu4"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.um_service_discovery_tg.arn
  }
}

resource "aws_lb_target_group" "um_service_discovery_tg" {
  name     = format("um-srv-discovery-%s-lb", var.env)
  port     = 8081
  protocol = "HTTP"
  vpc_id   = var.vpc_id

  target_type = "ip" # For ECS tasks you need to use IP to suiable mode "awsvpc" scale out
  
  health_check {
    # enabled             = true
    path                = "/healcheck"
    port                = "8081"
    # protocol            = "HTTP"
    # healthy_threshold   = 3
    # unhealthy_threshold = 2
    # interval            = 30
    # timeout             = 10
    matcher             = "200"
  }
}


resource "aws_lb_listener" "http_8082" {
  load_balancer_arn = aws_lb.um_api_gateway_service_lb.arn
  port              = "8082"
  protocol          = "HTTP"

  # ssl_policy        = "ELBSecurityPolicy-2016-08"
  # certificate_arn   = "arn:aws:iam::187416307283:server-certificate/test_cert_rab3wuqwgja25ct3n4jdj2tzu4"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.um_ui_tg.arn
  }
}

resource "aws_lb_target_group" "um_ui_tg" {
  name     = format("um-ui-%s-lb", var.env)
  port     = 8082
  protocol = "HTTP"
  vpc_id   = var.vpc_id

  target_type = "ip" # For ECS tasks you need to use IP to suiable mode "awsvpc" scale out
  
  health_check {
    # enabled             = true
    path                = "/"
    port                = "8082"
    # protocol            = "HTTP"
    # healthy_threshold   = 3
    # unhealthy_threshold = 2
    # interval            = 30
    # timeout             = 10
    matcher             = "200,201,202"
  }
}

resource "aws_lb_listener" "http_8083" {
  load_balancer_arn = aws_lb.um_api_gateway_service_lb.arn
  port              = "8083"
  protocol          = "HTTP"

  # ssl_policy        = "ELBSecurityPolicy-2016-08"
  # certificate_arn   = "arn:aws:iam::187416307283:server-certificate/test_cert_rab3wuqwgja25ct3n4jdj2tzu4"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.um_user_registration_tg.arn
  }
}

resource "aws_lb_target_group" "um_user_registration_tg" {
  name     = format("um-user-registration-%s-lb", var.env)
  port     = 8083
  protocol = "HTTP"
  vpc_id   = var.vpc_id

  target_type = "ip" # For ECS tasks you need to use IP to suiable mode "awsvpc" scale out
  
  health_check {
    # enabled             = true
    path                = "/healcheck"
    port                = "8083"
    # protocol            = "HTTP"
    # healthy_threshold   = 3
    # unhealthy_threshold = 2
    # interval            = 30
    # timeout             = 10
    matcher             = "200,201,202"
  }
}