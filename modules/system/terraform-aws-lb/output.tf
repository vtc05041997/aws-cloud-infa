output "um_api_gateway_service_lb" {
  value = aws_lb.um_api_gateway_service_lb.id
}

output "um_api_gateway_service_tg" {
  value = aws_lb_target_group.um_api_gateway_service_tg.arn
}

output "um_service_discovery_tg" {
  value = aws_lb_target_group.um_service_discovery_tg.arn
}

output "um_ui_tg" {
  value = aws_lb_target_group.um_ui_tg.arn
}

output "um_user_registration_tg" {
  value = aws_lb_target_group.um_user_registration_tg.arn
}