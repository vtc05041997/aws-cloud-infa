variable "env" {
  type = string
}

variable "public_subnets" {
  type = set(string)
}

variable "user_management_api_template_id" {
  type = string
}