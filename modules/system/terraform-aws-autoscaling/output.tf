output "user_management_api_asg_id" {
  value = aws_autoscaling_group.user_management_api_asg.id
}

output "user_management_api_asg_arn" {
  value = aws_autoscaling_group.user_management_api_asg.arn
}